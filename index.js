var oracledb = require('oracledb');
oracledb.initOracleClient({libDir: 'C:\\db\\oracle\\instantclient_21_3'});

const dbConfig = { user: 'u1', password: 'p2', connectString: 'u1.url.com:1533/qa' };


async function print() {

    const date = await getSysDate();

    if(typeof date !== "undefined")
    {
        console.log(date);
    } 
    
}

async function getSysDate() {
    let connection;
    let sql, options, result;
    
    try {
      connection = await oracledb.getConnection(dbConfig);

      sql = `select sysdate from dual`;

      const binds  = [];  
      options = {
        outFormat: oracledb.OBJECT
      };
  
      result = await connection.execute(sql, binds, options);

      return await result.rows[0].SYSDATE;
      
    } catch (err) {
      console.error(err);
    } finally {
      if (connection) {
        try {
      await connection.close();
        } catch (err) {
      console.error(err);
        }
      }
    }
  }
  
  print();